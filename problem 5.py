def gcd(a,b):
	while b:
		a, b = b, a%b
	return a

def lcm(a,b):
	return a*b // gcd(a,b)

def lcm_multiple(*args):
	print reduce(lcm, args)

lcm_multiple(*range(1,20))
